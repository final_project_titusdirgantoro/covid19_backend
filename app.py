from controller import covid19
from flask import Flask, jsonify, request
from flask_jwt_extended import JWTManager

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
app.config['JWT_SECRET_KEY'] = "testkunci"

jwt = JWTManager(app)


@app.route("/rumahsakit", methods=["GET"])
def showRS():
    param = request.json
    return covid19.showRumahSakit()

@app.route("/vaksinasi", methods=["GET"])
def showVaksin():
    param = request.json
    return covid19.showVaksinasi()

@app.route("/kasuscovid", methods=["GET"])
def showKasus():
    param = request.json
    return covid19.showKasusCovid()

@app.route("/kasuscovidprovinsi", methods=["POST"])
def showKasusProvinsi():
    params = request.json
    return covid19.showKasusCovidProvinsi(**params)

@app.route("/kasuscovidkodeprovinsi", methods=["POST"])
def showKasusKode():
    params = request.json
    return covid19.showKasusCovidKode(**params)

@app.route("/rumahsakitprovinsi", methods=["POST"])
def showRSprovinsi():
    param = request.json
    print(param, 'ini paramnya')
    return covid19.showRumahSakitProvinsi(**param)

@app.route("/rumahsakitnama", methods=["POST"])
def showRSnama():
    params = request.json
    return covid19.showRumahSakitNama(**params)

@app.route("/requesttoken", methods=["POST"])
def requestToken():
    params = request.json
    return covid19.token(**params)

@app.route("/insertakun", methods=["POST"])
def insertAkun():
    params = request.json
    return covid19.insertUser(**params)


if __name__ == "__main__":
    app.run(debug=True)