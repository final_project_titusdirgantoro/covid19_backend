# covid19_backend
# Mohon maaf yang sebesar besarnya karena saya telat dalam mengerjakan Tugas Akhir karena minggu ini sangat full pekerjaan
# Mohon untuk setidaknya Bapak/Ibu berkenan untuk memberikan kesempatan terkait pengumpulan Tugas Akhir saya. Terima kasih banyak Pak/Bu

Tujuan API:
API ini ditujukan untuk user yang membutuhkan informasi terkait penyebaran virus corona di Indonesia, vaksinasi di indonesia, dan Rumah sakit

Source API :
- Kasus Covid-19 = https://services5.arcgis.com/VS6HdKS0VfIhv8Ct/arcgis/rest/services/COVID19_Indonesia_per_Provinsi/FeatureServer/0/query?where=1%3D1&outFields=*&outSR=4326&f=json
- Vaksinasi = https://vaksincovid19-api.vercel.app/api/vaksin
- Rumash Sakit = https://dekontaminasi.com/api/id/covid19/hospitals

Tabel yang digunakan :
1. users
2. rumah_sakit
3. vaksinasi
4. kasus_covid

Fitur - fitur yang disediakan :
1. Kasus Covid-19
- Total semua kasus di seluruh provinsi
- Total kasus berdasarkan nama provinsi tertentu (butuh JWT)<br />
body :
{
    "provinsi":"Aceh"
}
- Total kasus berdasarkan kode provinsi (butuh JWT)<br />
body :
{
    "kode_provinsi":"11"
}

2. Vaksinasi 
- Total semua informasi terkait jumlah vaksinasi

3. Rumah Sakit Covid-19
- Rumah sakit berdasarkan provinsi (butuh JWT)<br />
body :
{
    "provinsi":"aceh"
}
- Seluruh informasi rumah sakit
- Rumah sakit berdasarkan nama (butuh JWT)<br />
body :
{
    "nama":"umum"
}

4. User
- Mendaftarkan email dan password
- Masuk menggunakan POST pada postman dan akan mendapatkan JWT

Cara penggunaan :
1. Buat database terlebih dahulu menggunakan MySQL Workbench
- Buka MySQL Workbench lalu klik dua kali pada root atau localhost di tampilan 'MySQL Connection'
- Masukan Password
- Klik "Create a new schema in the connected server"
- Masukan nama schema yaitu "covid19_backend"
- Klik "Apply" dan muncul pop-up lalu klik "Apply" dan "Finish"

2. Ketikan "git clone (url_project)" pada directory anda
3. buka cmd pada directory anda lalu jangan lupa untuk membuat Virtual Environment yang baru, lalu masuk ke dalam environment tersebut.
4. jalan kan "pip install -r requirements.txt" untuk menginstall semua library atau modul yang dibutuhkan pada project ini
5. Jalankan backend server dengan ketikan "python app.py"

6. Buka postman anda, lalu masukan "http://127.0.0.1:5000/requesttoken" dengan body (json) yaitu 

{
    "email": "admin",
    "password":"admin"
}

7. masukan token, dan anda bisa akses apa saja sesuai link serta body (json)
