from mysql.connector import connect
from urllib.request import urlopen
import json

from mysql.connector.errors import custom_error_exception

class database:
    # Melakukan koneksi ke database yang ada di server localhost
    def __init__(self):
        try:
            self.db = connect(host='localhost',
                            database='covid19_backend',
                            user='root',
                            password='yohanes321')
            if self.db.is_connected():
                print('Connected to MySQL database')
            print(self.create_table())
            print(self.insertAdmin())
            print(self.insertDataAPI())
        except Exception as e:
            print(e)
    
    # Melakukan pembuatan table untuk yang pertama kali
    def create_table(self):
        try :
            query_table = '''create table users (email varchar(20),password varchar(20));
            create table vaksinasi (total_sasaran int, sasaran_vaksin_sdmk int, sasaran_vaksin_lansia int, sasaran_vaksin_petugas int, vaksinasi_1 int, vaksinasi_2 int);
            create table rumah_sakit (nama varchar(100), alamat varchar(200), daerah varchar(100), nomor_telp varchar(200), provinsi varchar(100));
            create table kasus_covid (kode_provinsi int, provinsi varchar(100), kasus_positif int, kasus_sembuh int, kasus_meninggal int);
            '''
            cursor = self.db.cursor()
            cursor.execute(query_table)
            return "Berhasil membuat tabel"
        except :
            return "Gagal membuat tabel atau tabel sudah ada"
    
    # Memberikan data email dan password dan digunakan untuk verifikasi akun
    def showUserByEmailandPass(self, **params):
        cursor = self.db.cursor()
        query = '''
            select * 
            from users 
            where email = "{0}" and password = "{1}";
        '''.format(params["email"], params["password"])
        
        cursor.execute(query)
        result = cursor.fetchone()
        return result

    # Melakukan pengecekkan terhadap isi tabel
    def cekIsiTabel(self,tabel):
        cursor = self.db.cursor()
        query = ''' SELECT EXISTS(SELECT * from {0})
        '''.format(tabel)
        cursor.execute(query)
        result = cursor.fetchall()
        return result
    
    # Melakukan insert data pada database melalui API
    def insertDataAPI(self):
        print(self.insertDataVaksinAPI())
        print(self.insertDataKasusCovidAPI())
        print(self.insertDataRumahSakitAPI())

    # Memasukkan akun admin
    def insertAdmin(self):
        cursor = self.db.cursor()
        result = self.cekIsiTabel("users")
        if result[0][0] == 0:
            query = '''
                insert into 
                users (email,password) values ("admin","admin");
            '''
            cursor.execute(query)
            self.commitData()
            return "Berhasil membuat akun admin"
        return "Sudah terdapat admin"
    
    # Mengambil data json dari API untuk data vaksinasi
    def getVaksinAPI(self):
        url = "https://vaksincovid19-api.vercel.app/api/vaksin"
        response = urlopen(url)
        data_json = json.loads(response.read())
        result = self.cekIsiTabel("vaksinasi")
        if result[0][0] != 0 :
            return None
        else:
            return data_json
    
    # Mengambil data json dari API untuk data kasus covid
    def getKasusCovidAPI(self):
        url = "https://services5.arcgis.com/VS6HdKS0VfIhv8Ct/arcgis/rest/services/COVID19_Indonesia_per_Provinsi/FeatureServer/0/query?where=1%3D1&outFields=*&outSR=4326&f=json"
        response = urlopen(url)
        data_json = json.loads(response.read())
        result = self.cekIsiTabel("kasus_covid")
        if result[0][0] != 0 :
            return None
        else:
            return data_json

    # Mengambil data json dari API untuk data rumah sakit
    def getRumahSakitAPI(self):
        url = "https://dekontaminasi.com/api/id/covid19/hospitals"
        response = urlopen(url)
        data_json = json.loads(response.read())
        result = self.cekIsiTabel("rumah_sakit")
        if result[0][0] != 0 :
            return None
        else:
            return data_json
    
    # memasukan data dari API ke database local
    def insertDataVaksinAPI(self):
        cursor = self.db.cursor()
        data_json = self.getVaksinAPI()
        if data_json == None :
            return "Data Vaksinasi gagal ditambahkan melalui API"
        else:
            total_sasaran = data_json['totalsasaran']
            sasaran_vaksin_sdmk = data_json['sasaranvaksinsdmk']
            sasaran_vaksin_lansia = data_json['sasaranvaksinlansia']
            sasaran_vaksin_petugas = data_json['sasaranvaksinpetugaspublik']
            vaksinasi_1 = data_json['vaksinasi1']
            vaksinasi_2 = data_json['vaksinasi2']
            query = '''
            insert into vaksinasi (total_sasaran,sasaran_vaksin_sdmk,sasaran_vaksin_lansia,sasaran_vaksin_petugas,vaksinasi_1,vaksinasi_2) 
            values ({0},{1},{2},{3},{4},{5});
            '''.format(total_sasaran,sasaran_vaksin_sdmk,sasaran_vaksin_lansia,sasaran_vaksin_petugas,vaksinasi_1,vaksinasi_2)
            cursor.execute(query)
            self.commitData()
            return "Data Vaksinasi berhasil ditambahkan melalui API"

    # memasukan data dari API ke database local
    def insertDataKasusCovidAPI(self):
        cursor = self.db.cursor()
        data_json = self.getKasusCovidAPI()
        if data_json != None :
            data = data_json['features']
            # data = data[0]['attributes']
            for i in data:
                i = i['attributes']
                kode_provinsi1 = i['Kode_Provi']
                provinsi1 = i['Provinsi']
                kasus_positif1 = i['Kasus_Posi']
                kasus_sembuh1 = i['Kasus_Semb']
                kasus_meninggal1 = i['Kasus_Meni']
                query = "insert into kasus_covid (kode_provinsi, provinsi, kasus_positif, kasus_sembuh, kasus_meninggal) values ({0},'{1}',{2},{3},{4});".format(kode_provinsi1,provinsi1, kasus_positif1, kasus_sembuh1, kasus_meninggal1)
                cursor.execute(query)
                self.commitData()
            return "Data Kasus Covid berhasil ditambahkan melalui API"
        else:
            return "Data Kasus Covid gagal ditambahkan melalui API"

    # memasukan data dari API ke database local
    def insertDataRumahSakitAPI(self):
        cursor = self.db.cursor()
        data_json = self.getRumahSakitAPI()
        if data_json != None :
            for data in data_json:
                nama = data['name']
                alamat = data['address']
                daerah = data['region']
                nomor_telp = data['phone']
                provinsi = data['province']
                query = '''
                insert into rumah_sakit (nama,alamat,daerah,nomor_telp,provinsi)
                values ('{0}','{1}','{2}','{3}','{4}');
                '''.format(nama,alamat, daerah, nomor_telp, provinsi)
                cursor.execute(query)
                self.commitData()
            return "Data Rumah Sakit berhasil ditambahkan melalui API"
        else:
            return "Data Rumah Sakit gagal ditambahkan melalui API"
    
    def showDataVaksinasi(self):
        cursor = self.db.cursor()
        query ='''select * from vaksinasi'''
        cursor.execute(query)
        result = cursor.fetchall()
        return result
    
    def showDataKasusCovid(self):
        cursor = self.db.cursor()
        query ='''select * from kasus_covid'''
        cursor.execute(query)
        result = cursor.fetchall()
        return result

    def showDataRumahSakit(self):
        cursor = self.db.cursor()
        query ='''select * from rumah_sakit'''
        cursor.execute(query)
        result = cursor.fetchall()
        return result
    
    def showDataRumahSakitByProvinsi(self, **params):
        cursor = self.db.cursor()
        provinsi = params['provinsi'].capitalize()
        query = '''
            select * 
            from rumah_sakit 
            where provinsi = '{0}';
        '''.format(provinsi)
        
        cursor.execute(query)
        result = cursor.fetchall()
        return result

    def showDataRumahSakitByNama(self, **params):
        cursor = self.db.cursor()
        query = '''
            select * 
            from rumah_sakit 
            where nama LIKE '%{0}%';
        '''.format(params["nama"].upper())
        
        cursor.execute(query)
        result = cursor.fetchall()
        return result

    def showDataKasusCovidByProvinsi(self, **params):
        cursor = self.db.cursor()
        provinsi = params['provinsi'].capitalize()
        query = '''
            select * 
            from kasus_covid 
            where provinsi = '{0}';
        '''.format(provinsi)
        
        cursor.execute(query)
        result = cursor.fetchone()
        return result

    def showDataKasusCovidByKodeProvinsi(self, **params):
        cursor = self.db.cursor()
        kode = params['kode_provinsi']
        query = '''
            select * 
            from kasus_covid 
            where kode_provinsi = {0};
        '''.format(kode)
        
        cursor.execute(query)
        result = cursor.fetchone()
        return result
    
    def insertUserBaru(self, **params):
        try:
            cursor = self.db.cursor()
            email = params['email']
            password = params['password']
            query = '''
            insert into users (email,password) values ('{0}','{1}');
            '''.format(email,password)
            cursor.execute(query)
            self.commitData()
            return [email,password]
        except:
            return "Gagal menambahkan Akun"
    
    def commitData(self):
        self.db.commit()
    
    # menutup koneksi
    def closeConnection(self):
        if self.db is not None and self.db.is_connected():
            self.db.close()