from models.covid19 import database
from flask import Flask, jsonify, request
from flask_jwt_extended import *
import datetime

mysqldb = database()

# variabel expires dibuat untuk menentukan masa berlaku dari token kita
# create_access_token() adalah metode yang digunakan untuk membuat access_token kita.
def token(**params):
    dbresult = mysqldb.showUserByEmailandPass(**params)
    if dbresult is not None:
        user = {
            "email" : dbresult[0],
            "password" : dbresult[1]            
        }
        expires = datetime.timedelta(days=1)
        expires_refresh = datetime.timedelta(days=3)
        access_token = create_access_token(user, fresh=True, expires_delta=expires)
        
        data = {
            "data": user,
            "token_access": access_token
        }
    else:
        data = {
            "message":"Email tidak terdaftar"
        }
        
    return jsonify(data)


def showVaksinasi():
    dbresult = mysqldb.showDataVaksinasi()
    result = []
    for items in dbresult:
        vaksin = {
            "total_sasaran" : items[0],
            "sasaran_vaksin_sdmk" : items[1],
            "sasaran_vaksin_lansia" : items[2],
            "sasaran_vaksin_petugas" : items[3],
            "vaksinasi_1" : items[4],
            "vaksinasi_2" : items[5],
        }
        result.append(vaksin)
        
    return jsonify(result)


def showKasusCovid():
    dbresult = mysqldb.showDataKasusCovid()
    result = []
    for items in dbresult:
        kasus = {
            "kode_provinsi" : items[0],
            "provinsi" : items[1],
            "kasus_positif" : items[2],
            "kasus_sembuh" : items[3],
            "kasus_meninggal" : items[4],
        }
        result.append(kasus)
        
    return jsonify(result)


def showRumahSakit():
    dbresult = mysqldb.showDataRumahSakit()
    result = []
    for items in dbresult:
        rumahsakit = {
            "nama" : items[0],
            "alamat" : items[1],
            "daerah" : items[2],
            "nomor_telp" : items[3],
            "provinsi" : items[4],
        }
        result.append(rumahsakit)
        
    return jsonify(result)

@jwt_required()
def showRumahSakitProvinsi(**params):
    dbresult = mysqldb.showDataRumahSakitByProvinsi(**params)
    result = []
    if type(dbresult) == list :
        for item in dbresult:
            rumahsakit = {
                "nama" : item[0],
                "alamat" : item[1],
                "daerah" : item[2],
                "nomor_telp" : item[3],
                "provinsi" : item[4],            
            }
            result.append(rumahsakit)
    else :
        rumahsakit = {
                "nama" : dbresult[0][0],
                "alamat" : dbresult[0][1],
                "daerah" : dbresult[0][2],
                "nomor_telp" : dbresult[0][3],
                "provinsi" : dbresult[0][4],            
            }
        result.append(rumahsakit)
    return jsonify(result)

@jwt_required()
def showRumahSakitNama(**params):
    dbresult = mysqldb.showDataRumahSakitByNama(**params)
    result = []
    if type(dbresult) == list :
        for item in dbresult:
            rumahsakit = {
                "nama" : item[0],
                "alamat" : item[1],
                "daerah" : item[2],
                "nomor_telp" : item[3],
                "provinsi" : item[4],            
            }
            result.append(rumahsakit)
    else :
        rumahsakit = {
                "nama" : dbresult[0],
                "alamat" : dbresult[1],
                "daerah" : dbresult[2],
                "nomor_telp" : dbresult[3],
                "provinsi" : dbresult[4],            
            }
        result.append(rumahsakit)
        
    return jsonify(result)

@jwt_required()
def showKasusCovidProvinsi(**params):
    dbresult = mysqldb.showDataKasusCovidByProvinsi(**params)
    result = []
    kasus = {
        "kode_provinsi" : dbresult[0],
        "provinsi" : dbresult[1],
        "kasus_positif" : dbresult[2],
        "kasus_sembuh" : dbresult[3],
        "kasus_meninggal" : dbresult[4],
        }
    result.append(kasus)
        
    return jsonify(result)

@jwt_required()
def showKasusCovidKode(**params):
    dbresult = mysqldb.showDataKasusCovidByKodeProvinsi(**params)
    result = []
    kasus = {
        "kode_provinsi" : dbresult[0],
        "provinsi" : dbresult[1],
        "kasus_positif" : dbresult[2],
        "kasus_sembuh" : dbresult[3],
        "kasus_meninggal" : dbresult[4],
        }
    result.append(kasus)
        
    return jsonify(result)


def insertUser(**params):
    dbresult = mysqldb.insertUserBaru(**params)
    result = []
    hasil = dbresult,"Berhasil ditambahkan"
    result.append(hasil)
    return jsonify(result)

# def show(**params):
#     dbresult = mysqldb.showUserById(**params)
#     user = {
#         "id" : dbresult[0],
#         "username" : dbresult[1],
#         "firstname" : dbresult[2],
#         "lastname" : dbresult[3],
#         "email" : dbresult[4]            
#     }
        
#     return jsonify(user)